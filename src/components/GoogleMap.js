import React,{Component} from "react";
import GoogleMapReact from 'google-map-react';




class GoogleMap extends Component{
    constructor(props){
        super(props);
        this.state={
            center: {
                lat: this.props.lat,
                lng: this.props.lon
              },
              zoom: 11
            };
        
    }

    render(){
        return(
            <div style={{ height: '200px', width: '300px' }}>
        <GoogleMapReact
        bootstrapURLKeys={{key:"AIzaSyDBC_dH35Kn57m9CWmBLRvysNroM6hVkKo"}}
          defaultCenter={this.state.center}
          defaultZoom={this.state.zoom}
        >
        </GoogleMapReact>
      </div>

        )
    }
}
export default GoogleMap;