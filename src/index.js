import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/App";
import {Provider} from "react-redux";
import ReduxPromise from "redux-promise";
import {createStore,applyMiddleware} from "redux";
import registerServiceWorker from './registerServiceWorker';
import reducers from "./reducers"

const CreateStoreWithMiddleware=applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
    <Provider store={CreateStoreWithMiddleware(reducers)}>
         <App />
    </Provider>,
    
    document.getElementById('root'));
registerServiceWorker();
